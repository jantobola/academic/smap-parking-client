package com.jantobola.parking;

import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;
import java.io.IOException;
import java.util.Date;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * SendCoordsAsync
 *
 * @author Jan Tobola, 2016
 */
public class SendCoordsAsync extends AsyncTask<Location, Void, String> {

    private Context context;

    public SendCoordsAsync(Context context) {
        this.context = context;
    }

    @Override
    protected String doInBackground(final Location... params) {

        Location location = params[0];
        if (location != null) {

            // -----------------------

            Retrofit r = new Retrofit.Builder()
                    .baseUrl(Cfg.URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            Service service = r.create(Service.class);
            Response<ParkingResponse> response = null;

//            ParkingEntity e = new ParkingEntity();
//            e.setDeviceName(Build.MODEL);
//            e.setLat(location.getLatitude());
//            e.setLon(location.getLongitude());
//            e.setParkingTime(new Date());

            try {
                response = service
                        .saveLocation(Build.MODEL, String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()))
                        .execute();
            } catch (IOException er) {
                er.printStackTrace();
            }

            // -------------------------

            if (response.code() != 200) {
                Log.e("REQ", "Cannot save location on server.");
                return response.message();
            }

            return response.body().getMessage();
        }

        return "Failed";
    }

    @Override
    protected void onPostExecute(final String s) {
        super.onPostExecute(s);
        Toast.makeText(context, "Response: " + s, Toast.LENGTH_SHORT).show();
    }

    public interface Service {

        @FormUrlEncoded
        @POST("/api/save")
        Call<ParkingResponse> saveLocation(
                @Field("deviceName") String deviceName,
//                @Field("parkingTime") String parkingTime,
                @Field("lat") String lat,
                @Field("lon") String lon
        );

    }

}
