package com.jantobola.parking;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 *
 */
public class MainActivity extends Activity {

    private Button parkButton;
    private LocationManager locationManager;
    private Location mLocation;
    private TextView textInfo;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        textInfo = ((TextView) findViewById(R.id.textInfo));
        textInfo.setText("GPS no info");

        turnGPSOn();
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, new LocationListener() {
            public void onLocationChanged(final Location location) {
                textInfo.setText("Location changed: " + location.getLatitude() + ", " + location.getLongitude());
                mLocation = location;
            }

            public void onStatusChanged(final String provider, final int status, final Bundle extras) {

            }

            public void onProviderEnabled(final String provider) {

            }

            public void onProviderDisabled(final String provider) {

            }
        });

        locationManager.addGpsStatusListener(new GpsStatus.Listener() {
            public void onGpsStatusChanged(final int event) {
                switch (event) {
                    case GpsStatus.GPS_EVENT_FIRST_FIX:
                        flash("Event First fix");
                        break;
//                    case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
//                        flash("Event Satelite status");
//                        break;
                    case GpsStatus.GPS_EVENT_STARTED:
                        flash("Event Started");
                        break;
                    case GpsStatus.GPS_EVENT_STOPPED:
                        flash("Event Stopped");
                        break;

                }
            }
        });

        parkButton = ((Button) findViewById(R.id.parking_button));
        parkButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {
//                if(mLocation != null) {
                    Location l = new Location("gps");
                    l.setLatitude(1.1);
                    l.setLongitude(2.2);
                    mLocation = l;
                    Log.i("UI", "Parking button clicked, running async task.");
                    flash("Getting current location, saving...");
                    new SendCoordsAsync(getApplicationContext()).execute(mLocation);
                    flash("Saved location: " + mLocation.getLatitude() + ", " + mLocation.getLongitude());
//                } else {
//                    flash("Unknown location.");
//                }
            }
        });
    }

    private void flash(String s) {
        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
    }

    private void turnGPSOn(){
        String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        if(!provider.contains("gps")){ //if gps is disabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            sendBroadcast(poke);
        }
    }


}
